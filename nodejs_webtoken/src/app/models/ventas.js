//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VentasSchema = new Schema({

	fecha : String,
	hora : String,
	clientesId :String,
	empleadoId : String
});

module.exports = mongoose.model('Ventas', VentasSchema);