//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientesSchema = new Schema({
	
	nom : String,
	dire : String,
	tel : String,
	sex : String
});

module.exports = mongoose.model('Clientes', ClientesSchema);